package spaceTanks.rendering.states;

import java.io.IOException;
import java.nio.file.Paths;

import org.jsfml.graphics.*;
import org.jsfml.window.event.Event;
import spaceTanks.rendering.gui.*;

public class MainMenuState extends State {

	Texture background = new Texture();
	Sprite sBackground = new Sprite();
	
	Font font = new Font();
	
	ICallable afterPushButton = new ICallable()	{

		@Override
		public void toCallAfterPush(float dt) {
			returnState = new PlayerSelectState();
		}
		
	};
	
	ICallable afterPushExit = new ICallable()	{

		@Override
		public void toCallAfterPush(float dt) {
			returnState = null;
		}
		
	};
	
	public MainMenuState()	{
		
		try {
			background.loadFromFile(Paths.get("assets/MainMenuBackground.jpg"));
		} 
		catch(IOException e)	{
			System.out.println("MainMenuBackground not found");
		}
		sBackground.setTexture(background);
		returnState = this;
		
		try {
			font.loadFromFile(Paths.get("assets/font.ttf"));
		} catch (IOException e) {
			System.out.println("Font not found");
		}
		Label label = new Label("SPACE TANKS", font, 128, Color.WHITE, TextStyle.REGULAR);
		label.setAnchorMode(AnchorMode.UP);
		label.setOffSet(0, 100);
		canvas.addWidget(label);
		
		Button button = new Button(100, 20, "NEW GAME", font, 60, Color.WHITE, TextStyle.REGULAR);
		button.setAnchorMode(AnchorMode.MIDDLE);
		button.setICallable(afterPushButton);
		canvas.addWidget(button);
		
		Button exitButton = new Button(100, 20, "EXIT", font, 60, Color.WHITE, TextStyle.REGULAR);
		exitButton.setAnchorMode(AnchorMode.MIDDLE);
		exitButton.setOffSet(0, 100);
		exitButton.setICallable(afterPushExit);
		canvas.addWidget(exitButton);
	}
	
	@Override
	public void handleEvent(Event event) {
		
		super.handleEvent(event);
		
		
	}

	@Override
	public State update(float dt) {
		canvas.update(dt);
		return returnState;
	}

	public void draw(RenderWindow window)	{
		window.draw(sBackground);
		canvas.draw(window);
	}

	
	@Override
	public void resize(int width, int height) {
		
	}

}
