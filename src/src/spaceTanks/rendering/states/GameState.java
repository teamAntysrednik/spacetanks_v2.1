package spaceTanks.rendering.states;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import org.jsfml.graphics.*;
import org.jsfml.window.event.Event;

import spaceTanks.gameLogic.GameEntity;
import spaceTanks.gameLogic.GameWorld;
import spaceTanks.gameLogic.ITerrainChange;
import spaceTanks.gameLogic.MapSize;
import spaceTanks.rendering.gui.*;

public class GameState extends State{
	
	Font font = new Font();
	GameWorld gameWorld;
	View view;
	

	RenderTexture planetRenderTexture, normalRenderTexture;
	Shader planetTerrainShader, normalShader, planetShader;
	
	float sunRadius = 2000;
	float time = 0;
	float ax, ay, az;
	float bx, by;
	
	CircleShape planet = new CircleShape();
	ArrayList<Texture> planetTextures = new ArrayList<Texture>();

	Sprite planets = new Sprite();
	
	RectangleShape tank;
	ArrayList<Texture> tankTextures = new ArrayList<Texture>();
	
	ArrayList<Texture> tankTextures2 = new ArrayList<Texture>();

	CircleShape currentPlayerCircle;
	RectangleShape cannon;
	
	CircleShape projectile;
	RectangleShape projectileIndicator;
	
	ArrayList<Label> hpLabels = new ArrayList<Label>();

	
	public GameState(MapSize size, int numberOfPlayers)	{
		returnState = this;
		
		gameWorld = new GameWorld(size, numberOfPlayers);
	
		
		planetRenderTexture = new RenderTexture();
		try {
			planetRenderTexture.create(size.getX(), size.getY());
		} catch (TextureCreationException e2) {
			e2.printStackTrace();
		}
		planetRenderTexture.setView(new View(new org.jsfml.system.Vector2f(0, 0), new org.jsfml.system.Vector2f(size.getX(), size.getY())));
		
		planetTerrainShader = new Shader();
		try {
			planetTerrainShader.loadFromFile(Paths.get("shaders/PlanetTerrain.vert"), Paths.get("shaders/PlanetTerrain.frag"));
		} catch (IOException | ShaderSourceException e1) {
			e1.printStackTrace();
		}
		
		normalRenderTexture = new RenderTexture();
		try {
			normalRenderTexture.create(size.getX(), size.getY());
		} catch(TextureCreationException e) {
			e.printStackTrace();
		}
		normalRenderTexture.setView(new View(new org.jsfml.system.Vector2f(0, 0), new org.jsfml.system.Vector2f(size.getX(), size.getY())));
		
		normalShader = new Shader();
		try {
			normalShader.loadFromFile(Paths.get("shaders/Normal.vert"), Paths.get("shaders/Normal.frag"));
		} catch ( IOException | ShaderSourceException e) {
			e.printStackTrace();
		}
		
		planetShader = new Shader();
		try {
			planetShader.loadFromFile(Paths.get("shaders/Planet.vert"), Paths.get("shaders/Planet.frag"));
		} catch ( IOException | ShaderSourceException e) {
			e.printStackTrace();
		}
		
		planetShader.setParameter("width", (float) size.getX()/2);
		planetShader.setParameter("height", (float) size.getY()/2);
		planetShader.setParameter("texture", Shader.CURRENT_TEXTURE);
		planetShader.setParameter("normalTexture", normalRenderTexture.getTexture());
		
		normalRenderTexture.clear();
		
		for(GameEntity entityPlanet : gameWorld.listOfPlanets)
		{
			planet.setRadius(entityPlanet.planetData.getRadius());
			planet.setOrigin(planet.getRadius(), planet.getRadius());
			planet.setPosition(entityPlanet.position.getPositionX(), entityPlanet.position.getPositionY());
			
			normalShader.setParameter("radius", entityPlanet.planetData.getRadius());
			normalShader.setParameter("middle", entityPlanet.position.getPositionX(), entityPlanet.position.getPositionY());
			
			normalRenderTexture.draw(planet, new RenderStates(normalShader));
		}
		
		normalRenderTexture.display();
		
		ax = ThreadLocalRandom.current().nextFloat() * 2 - 1;
		ay = ThreadLocalRandom.current().nextFloat() * 2 - 1;
		az = ThreadLocalRandom.current().nextFloat() + 1;
		
		bx = ay;
		by = -ax;
		
		float aLength = (float) Math.sqrt(ax * ax + ay * ay + az * az);
		float bLength = (float) Math.sqrt(bx * bx + by * by);
		
		ax /= aLength;
		ay /= aLength;
		az /= aLength;
		
		bx /= bLength;
		by /= bLength;
		
		for(int i = 0; i < 5; ++i)	{
			tankTextures.add(new Texture());
			tankTextures2.add(new Texture());
		}
		
		try {
			tankTextures.get(0).loadFromFile(Paths.get("assets/czolg1.jpg"));
			tankTextures.get(1).loadFromFile(Paths.get("assets/czolg2.jpg"));
			tankTextures.get(2).loadFromFile(Paths.get("assets/czolg3.jpg"));
			tankTextures.get(3).loadFromFile(Paths.get("assets/czolg4.jpg"));
			tankTextures.get(4).loadFromFile(Paths.get("assets/czolg5.jpg"));
			
			tankTextures2.get(0).loadFromFile(Paths.get("assets/cz1.jpg"));
			tankTextures2.get(1).loadFromFile(Paths.get("assets/cz2.jpg"));
			tankTextures2.get(2).loadFromFile(Paths.get("assets/cz3.jpg"));
			tankTextures2.get(3).loadFromFile(Paths.get("assets/cz4.jpg"));
			tankTextures2.get(4).loadFromFile(Paths.get("assets/cz5.jpg"));
		} 
		catch(IOException e)	{
			System.out.println("Tank's image not found");
		}
		
		
		try {
			planetTextures.add(new Texture());
			planetTextures.get(0).loadFromFile(Paths.get("assets/planet0.jpg"));
		} 
		catch(IOException e)	{
			System.out.println("Planet not found");
		}
		
		planet.setTexture(planetTextures.get(0));
		
		gameWorld.getTerrain().setCallback(new ITerrainChange(){

			@Override
			public void callback(Texture texture) {
				planetRenderTexture.clear();

				planetTerrainShader.setParameter("width", (float) size.getX()/2);
				planetTerrainShader.setParameter("height", (float) size.getY()/2);
				planetTerrainShader.setParameter("texture", texture);
				planetTerrainShader.setParameter("planetTexture", Shader.CURRENT_TEXTURE);
				
				for(GameEntity entityPlanet : gameWorld.listOfPlanets)
				{
					planet.setRadius(entityPlanet.planetData.getRadius());
					planet.setOrigin(planet.getRadius(), planet.getRadius());
					planet.setPosition(entityPlanet.position.getPositionX(), entityPlanet.position.getPositionY());
					
					planetRenderTexture.draw(planet, new RenderStates(planetTerrainShader));
				}
				
				planetRenderTexture.display();
				
			}
		});
		
		gameWorld.getTerrain().callCallback();
		
		planets.setOrigin(size.getX()/2, size.getY()/2);
		planets.setTexture(planetRenderTexture.getTexture());
		
		tank = new RectangleShape(new org.jsfml.system.Vector2f(15f, 30f));
		tank.setOrigin(5f, 15f);
		
		currentPlayerCircle = new CircleShape(25f);
		currentPlayerCircle.setOrigin(25f, 25f);
		currentPlayerCircle.setFillColor(Color.TRANSPARENT);
		currentPlayerCircle.setOutlineColor(Color.YELLOW);
		currentPlayerCircle.setOutlineThickness(3.0f);
		
		cannon = new RectangleShape(new org.jsfml.system.Vector2f(25f, 2f));
		cannon.setOrigin(0f, 1f);
		
		projectile = new CircleShape(3f);
		projectile.setOrigin(new org.jsfml.system.Vector2f(3f, 3f));
		projectile.setFillColor(Color.YELLOW);
		
		projectileIndicator = new RectangleShape(new org.jsfml.system.Vector2f(20f, 3f));
		projectileIndicator.setOrigin(new org.jsfml.system.Vector2f(-10f, 1.5f));
		projectileIndicator.setFillColor(Color.RED);
		
		try {
			font.loadFromFile(Paths.get("assets/font.ttf"));
		} catch (IOException e) {
			System.out.println("Font not found");
		}
		
		int shiftLabel = 150;
		for(GameEntity entityTank : gameWorld.listOfTanks)	{
			Label label = new Label( ((Integer) entityTank.tankData.getHp()).toString() , font, 20, Color.WHITE, TextStyle.REGULAR);
			label.setAnchorMode(AnchorMode.UPPER_LEFT);
			label.setOffSet(shiftLabel, 20);
			shiftLabel += 150;
			
			hpLabels.add(label);
			canvas.addWidget(label);
		}
		
		shiftLabel = 100;
		int nrTank = 0;
		int cTanks = gameWorld.listOfTanks.size();
		for(Texture tankTT : tankTextures2)	{
			if(nrTank >= cTanks)	{
				break;
			}
			nrTank++;
			
			RectangleShape tankT = new RectangleShape(new org.jsfml.system.Vector2f(40, 20 ) );
			tankT.setOrigin(
					tankT.getLocalBounds().width / 2, 
					tankT.getLocalBounds().height / 2);
			tankT.setTexture(tankTT);
			
			Button B = new Button(30, 30, "", font, 30, Color.WHITE, TextStyle.REGULAR);
			B.setAnchorMode(AnchorMode.UPPER_LEFT);
			B.setOffSet(shiftLabel, 20);
			B.setButtonBackground(tankT);
			B.setRepeat(true);
			shiftLabel+=150;
			canvas.addWidget(B);
		}
		
		Label labelCN = new Label( "CANNON MOVE", font, 18, Color.WHITE, TextStyle.REGULAR);
		labelCN.setAnchorMode(AnchorMode.DOWN);
		labelCN.setOffSet(-131, -43);
		canvas.addWidget(labelCN);
		
		RectangleShape button1Rectangle = new RectangleShape(new org.jsfml.system.Vector2f(40, 30) );
		button1Rectangle.setOrigin(
				button1Rectangle.getLocalBounds().width / 2, 
				button1Rectangle.getLocalBounds().height / 2);
		button1Rectangle.setFillColor(Color.BLUE);
				
		Button button1 = new Button(40, 30, "<", font, 30, Color.WHITE, TextStyle.REGULAR);
		button1.setAnchorMode(AnchorMode.DOWN);
		button1.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				gameWorld.moveCannon(true, dt);
			}
			
		});
		button1.setOffSet(-160, -18);
		button1.setButtonBackground(button1Rectangle);
		button1.setRepeat(true);
		canvas.addWidget(button1);
		
		RectangleShape button2Rectangle = new RectangleShape(new org.jsfml.system.Vector2f(40, 30) );
		button2Rectangle.setOrigin(
				button2Rectangle.getLocalBounds().width / 2, 
				button2Rectangle.getLocalBounds().height / 2);
		button2Rectangle.setFillColor(Color.BLUE);

		Button button2 = new Button(30, 30, ">", font, 30, Color.WHITE, TextStyle.REGULAR);
		button2.setAnchorMode(AnchorMode.DOWN);
		button2.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				gameWorld.moveCannon(false, dt);
			}
			
		});
		button2.setOffSet(-100, -18);
		button2.setButtonBackground(button2Rectangle);
		button2.setRepeat(true);
		canvas.addWidget(button2);
		
		Label labelTM = new Label( "TANK MOVE", font, 18, Color.WHITE, TextStyle.REGULAR);
		labelTM.setAnchorMode(AnchorMode.DOWN);
		labelTM.setOffSet(131, -43);
		canvas.addWidget(labelTM);
		
		
		
		
		RectangleShape button3Rectangle = new RectangleShape(new org.jsfml.system.Vector2f(40, 30 ) );
		button3Rectangle.setOrigin(
				button3Rectangle.getLocalBounds().width / 2, 
				button3Rectangle.getLocalBounds().height / 2);
		button3Rectangle.setFillColor(Color.BLUE);

		
		Button button3 = new Button(40, 30, "<", font, 30, Color.WHITE, TextStyle.REGULAR);
		button3.setAnchorMode(AnchorMode.DOWN);
		button3.setRepeat(true);

		button3.setICallable(new ICallable()	{
		
			@Override
			public void toCallAfterPush(float dt) {
				gameWorld.moveTank(true, dt);
			}
			
		});
		button3.setOffSet(100, -18);
		button3.setButtonBackground(button3Rectangle);
		canvas.addWidget(button3);
		 
		RectangleShape button4Rectangle = new RectangleShape(new org.jsfml.system.Vector2f(40, 30) );
		button4Rectangle.setOrigin(
				button4Rectangle.getLocalBounds().width / 2, 
				button4Rectangle.getLocalBounds().height / 2);
		button4Rectangle.setFillColor(Color.BLUE);
		
		Button button4 = new Button(40, 30, ">", font, 30, Color.WHITE, TextStyle.REGULAR);
		button4.setAnchorMode(AnchorMode.DOWN);
		button4.setRepeat(true);
		button4.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				gameWorld.moveTank(false, dt);
			}
			
		});
		button4.setOffSet(160, -18);
		button4.setButtonBackground(button4Rectangle);
		canvas.addWidget(button4);
		
		
		RectangleShape fireButtonRectangle = new RectangleShape(new org.jsfml.system.Vector2f(80, 40) );
		fireButtonRectangle.setOrigin(
				fireButtonRectangle.getLocalBounds().width / 2, 
				fireButtonRectangle.getLocalBounds().height / 2);
		fireButtonRectangle.setFillColor(Color.RED);
		
		Button fireButton = new Button(80, 40, "FIRE", font, 30, Color.WHITE, TextStyle.BOLD);
		fireButton.setAnchorMode(AnchorMode.DOWN);
		fireButton.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				gameWorld.shoot();
			}
			
		});
		fireButton.setOffSet(0, -30);
		fireButton.setButtonBackground(fireButtonRectangle);
		canvas.addWidget(fireButton);
		
		view = new View(new org.jsfml.system.Vector2f(0,0), new org.jsfml.system.Vector2f(0,0));
		resize(State.getWindow().getSize().x, State.getWindow().getSize().y);
		
	}
	
	
	@Override
	public void handleEvent(Event event) {
		super.handleEvent(event);
	}

	
	@Override
	public State update(float dt) {
		time += dt;
		gameWorld.update(dt);
		canvas.update(dt);
		returnState = this;
		if(gameWorld.checkEnd())	{
			Button button = new Button(300, 300, "GAME OVER!", font, 50, Color.WHITE, TextStyle.REGULAR);
			button.setAnchorMode(AnchorMode.MIDDLE);
			button.setOffSet(0, -200);
			canvas.addWidget(button);
			
		}
		return returnState;
	}

	
	@Override
	public void draw(RenderWindow window) {		
		View recentView = (View) window.getView();
		window.setView(view);
		
		float cos = (float) Math.cos(time/10);
		float sin = (float) Math.sin(time/10);
		planetShader.setParameter("lightPos", sunRadius * (cos * ax + sin * bx), sunRadius * (cos * ay + sin * by), sunRadius * cos * az);
		
		window.draw(planets, new RenderStates(planetShader));
		
		//int currentPlayer = 0;
		
		currentPlayerCircle.setPosition(gameWorld.listOfTanks.get(gameWorld.getCurrentPlayer()).position.getPositionX(), gameWorld.listOfTanks.get(gameWorld.getCurrentPlayer()).position.getPositionY());
		window.draw(currentPlayerCircle);

		for(GameEntity entityTank : gameWorld.listOfTanks)
		{			
			tank.setTexture(tankTextures.get(entityTank.tankData.getIdNumber()));
			
			tank.setRotation(entityTank.tankData.getAngle());
			tank.setPosition(entityTank.position.getPositionX(), entityTank.position.getPositionY());
			
			cannon.setRotation(entityTank.tankData.getAngle() + entityTank.tankData.getCannonAngle());
			cannon.setPosition(entityTank.position.getPositionX(), entityTank.position.getPositionY());
			
			window.draw(tank);
			window.draw(cannon);
			
		
				for(int i = 0; i < gameWorld.listOfTanks.size(); ++i)	{
					hpLabels.get(gameWorld.listOfTanks.get(i).tankData.getIdNumber()).setLabel(((Integer) gameWorld.listOfTanks.get(i).tankData.getHp()).toString() );
				}
				
			
			//currentPlayer += 1;
		}
		
		
		if(gameWorld.listOfProjectiles.size() != 0)	{
			projectile.setPosition(gameWorld.listOfProjectiles.get(0).position.getPositionX(), 
									gameWorld.listOfProjectiles.get(0).position.getPositionY());
			window.draw(projectile);
			
			int angle = 0;
			for(int i = 0; i < 4; ++i)
			{
				projectileIndicator.setPosition(projectile.getPosition());
				projectileIndicator.setRotation(angle);
				angle += 90;
				window.draw(projectileIndicator);
			}
		}
		
		window.setView(recentView);
		canvas.draw(window);
		
		
		
	}


	@Override
	public void resize(int width, int height) {
		int panelX = gameWorld.getPanelWidth();
		int panelY = gameWorld.getPanelHeight();
		
		int windowX = State.getWindow().getSize().x;
		int windowY =  State.getWindow().getSize().y;
		
		float aspectRatioPanel = (float) panelX / panelY;
		float aspectRatioWindow = (float) windowX / windowY;

		if(aspectRatioPanel == aspectRatioWindow){
			view.setSize(panelX, panelY);
			return;
		}
		
		if(aspectRatioPanel < aspectRatioWindow){
			float dx = ((float)windowX / windowY) * panelY - (float)panelX;
			view.setSize(panelX + dx, panelY);
			return;
		}
		
		if(aspectRatioPanel > aspectRatioWindow){
			float dY = ((float)windowY / windowX) * panelX - (float)panelY;
			view.setSize(panelX, panelY + dY);
			return;
		}
	}
	

}
