package spaceTanks.rendering.states;

import org.jsfml.graphics.RenderWindow;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.SizeEvent;

import spaceTanks.rendering.gui.Canvas;

abstract public class State {
	
	public State returnState;
	protected Canvas canvas;

	private static RenderWindow window;

	protected State()	{
		returnState = this;
		canvas = new Canvas(window.getSize().x, window.getSize().y);
	}
	
	public void handleEvent(Event event)	{
		canvas.handleEvent(event);
		
		if(event.type == Event.Type.RESIZED)	{
			SizeEvent sizeEvent = (SizeEvent) event;
			resize(sizeEvent.size.x, sizeEvent.size.y);
			
		}
	}
	
	public static void setWindow(RenderWindow w)	{
		window = w;
	}

	public static RenderWindow getWindow()	{
		return window;
	}
	
	public abstract State update(float dt);
	public abstract void draw(RenderWindow window);
	
	public abstract void resize(int width, int height);
}
