package spaceTanks.rendering.states;

import java.io.IOException;
import java.nio.file.Paths;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Font;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.TextStyle;
import org.jsfml.graphics.Texture;
import org.jsfml.window.event.Event;

import spaceTanks.gameLogic.MapSize;
import spaceTanks.rendering.gui.AnchorMode;
import spaceTanks.rendering.gui.Button;
import spaceTanks.rendering.gui.ICallable;
import spaceTanks.rendering.gui.Label;

public class PlayerSelectState extends State {

	Texture background = new Texture();
	Sprite sBackground = new Sprite();
	
	Font font = new Font();
	MapSize choosenSize = MapSize.MEDIUM;
	int numberOfPlayers = 2;
	

	public PlayerSelectState()	{
		
		try {
			background.loadFromFile(Paths.get("assets/MainMenuBackground.jpg"));
		} 
		catch(IOException e)	{
			System.out.println("MainMenuBackground not found");
		}
		sBackground.setTexture(background);
		returnState = this;
		
		try {
			font.loadFromFile(Paths.get("assets/font.ttf"));
		} catch (IOException e) {
			System.out.println("Font not found");
		}
		
		
		RectangleShape clickRectangle = new RectangleShape(new org.jsfml.system.Vector2f(60, 30) );
		clickRectangle.setOrigin(
				clickRectangle.getLocalBounds().width / 2, 
				clickRectangle.getLocalBounds().height / 2);
		clickRectangle.setFillColor(Color.RED);
		
		
		
		
		Label gameName = new Label("SPACE TANKS", font, 50, Color.WHITE, TextStyle.REGULAR);
		gameName.setAnchorMode(AnchorMode.UP);
		gameName.setOffSet(0, 50 );
		canvas.addWidget(gameName);
		
		Label label = new Label("Choose map size", font, 40, Color.WHITE, TextStyle.REGULAR);
		label.setAnchorMode(AnchorMode.UPPER_LEFT);
		label.setOffSet(200, 120 );
		canvas.addWidget(label);
		
		Button button = new Button(100, 20, "NEW GAME", font, 55 , Color.WHITE, TextStyle.REGULAR);
		button.setAnchorMode(AnchorMode.DOWN);
		button.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				returnState = new GameState(choosenSize, numberOfPlayers);

			}
			
		});
		button.setOffSet(0, -50);
		canvas.addWidget(button);

		Button tiny = new Button(100, 20, "TINY", font, 30, Color.WHITE, TextStyle.REGULAR);
		Button small = new Button(100, 20, "SMALL", font, 30, Color.WHITE, TextStyle.REGULAR);
		Button medium = new Button(100, 20, "MEDIUM", font, 30, Color.WHITE, TextStyle.REGULAR);
		Button large = new Button(100, 20, "LARGE", font, 30, Color.WHITE, TextStyle.REGULAR);
		Button huge = new Button(100, 20, "HUGE", font, 30, Color.WHITE, TextStyle.REGULAR);

		medium.setButtonBackgroundAfterClick();

		tiny.setAnchorMode(AnchorMode.UPPER_LEFT);
		tiny.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				choosenSize = MapSize.TINY;
				tiny.setButtonBackgroundAfterClick();
				small.cancelBackgroundAfterClick();
				medium.cancelBackgroundAfterClick();
				large.cancelBackgroundAfterClick();
				huge.cancelBackgroundAfterClick();
			}
			
		});
		tiny.setOffSet(200, 180);
		canvas.addWidget(tiny);
			
		small.setAnchorMode(AnchorMode.UPPER_LEFT);
		small.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				choosenSize = MapSize.SMALL;
				tiny.cancelBackgroundAfterClick();
				small.setButtonBackgroundAfterClick();
				medium.cancelBackgroundAfterClick();
				large.cancelBackgroundAfterClick();
				huge.cancelBackgroundAfterClick();
			}
			
		});
		small.setOffSet(200, 230);
		canvas.addWidget(small);
		
		medium.setAnchorMode(AnchorMode.UPPER_LEFT);
		medium.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				choosenSize = MapSize.MEDIUM;
				tiny.cancelBackgroundAfterClick();
				small.cancelBackgroundAfterClick();
				medium.setButtonBackgroundAfterClick();
				large.cancelBackgroundAfterClick();
				huge.cancelBackgroundAfterClick();
			}
			
		});
		medium.setOffSet(200, 280);
		canvas.addWidget(medium);
		
		large.setAnchorMode(AnchorMode.UPPER_LEFT);
		large.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				choosenSize = MapSize.LARGE;
				tiny.cancelBackgroundAfterClick();
				small.cancelBackgroundAfterClick();
				medium.cancelBackgroundAfterClick();
				large.setButtonBackgroundAfterClick();
				huge.cancelBackgroundAfterClick();
			}
			
		});
		large.setOffSet(200, 330) ;
		canvas.addWidget(large);
		
		huge.setAnchorMode(AnchorMode.UPPER_LEFT);
		huge.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				choosenSize = MapSize.HUGE;
				tiny.cancelBackgroundAfterClick();
				small.cancelBackgroundAfterClick();
				medium.cancelBackgroundAfterClick();
				large.cancelBackgroundAfterClick();
				huge.setButtonBackgroundAfterClick();
			}
			
		});
		huge.setOffSet(200, 380);
		canvas.addWidget(huge);
		
		Label cPlayers = new Label("Choose number of players", font, 40, Color.WHITE, TextStyle.REGULAR);
		cPlayers.setAnchorMode(AnchorMode.UPPER_LEFT);
		cPlayers.setOffSet(745,120);
		canvas.addWidget(cPlayers);
		
		Button twoPlayers = new Button(100, 20, "2 PLAYERS", font, 30, Color.WHITE, TextStyle.REGULAR);
		Button threePlayers = new Button(100, 20, "3 PLAYERS", font, 30, Color.WHITE, TextStyle.REGULAR);
		Button fourPlayers = new Button(100, 20, "4 PLAYERS", font, 30, Color.WHITE, TextStyle.REGULAR);
		Button fivePlayers = new Button(100, 20, "5 PLAYERS", font, 30, Color.WHITE, TextStyle.REGULAR);

		
		twoPlayers.setButtonBackgroundAfterClick();
		twoPlayers.setAnchorMode(AnchorMode.UPPER_LEFT);
		twoPlayers.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				numberOfPlayers = 2;
				twoPlayers.setButtonBackgroundAfterClick();
				threePlayers.cancelBackgroundAfterClick();
				fourPlayers.cancelBackgroundAfterClick();
				fivePlayers.cancelBackgroundAfterClick();
			}
			
		});
		twoPlayers.setOffSet(700, 180);
		canvas.addWidget(twoPlayers);
		
		threePlayers.setAnchorMode(AnchorMode.UPPER_LEFT);
		threePlayers.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				numberOfPlayers = 3;
				twoPlayers.cancelBackgroundAfterClick();
				threePlayers.setButtonBackgroundAfterClick();
				fourPlayers.cancelBackgroundAfterClick();
				fivePlayers.cancelBackgroundAfterClick();
			}
			
		});
		threePlayers.setOffSet(700, 230);
		canvas.addWidget(threePlayers);
		
		fourPlayers.setAnchorMode(AnchorMode.UPPER_LEFT);
		fourPlayers.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				numberOfPlayers = 4;
				twoPlayers.cancelBackgroundAfterClick();
				threePlayers.cancelBackgroundAfterClick();
				fourPlayers.setButtonBackgroundAfterClick();
				fivePlayers.cancelBackgroundAfterClick();
			}
			
		});
		fourPlayers.setOffSet(700, 280);
		canvas.addWidget(fourPlayers);
		
		fivePlayers.setAnchorMode(AnchorMode.UPPER_LEFT);
		fivePlayers.setICallable(new ICallable()	{

			@Override
			public void toCallAfterPush(float dt) {
				numberOfPlayers = 5;
				twoPlayers.cancelBackgroundAfterClick();
				threePlayers.cancelBackgroundAfterClick();
				fourPlayers.cancelBackgroundAfterClick();
				fivePlayers.setButtonBackgroundAfterClick();
			}
			
		});
		fivePlayers.setOffSet(700, 330);
		canvas.addWidget(fivePlayers);
		
	}
	
	public void handleEvent(Event event) {
		super.handleEvent(event);
	
	}
	@Override
	public State update(float dt) {
		canvas.update(dt);
		return returnState;
	}

	@Override
	public void draw(RenderWindow window)	{
		window.draw(sBackground);
		canvas.draw(window);
	}


	@Override
	public void resize(int width, int height) {
		
	}
	
}
