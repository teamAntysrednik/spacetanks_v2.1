package spaceTanks.rendering;

import org.jsfml.graphics.*;
import org.jsfml.system.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;

import spaceTanks.rendering.states.*;

public class Game {
	ContextSettings settings;
	public RenderWindow window;
	State currentState;

	Game()	{
		settings = new ContextSettings(8);
		window = new RenderWindow(new VideoMode(1024, 600), "SpaceTanks", 6, settings);
		State.setWindow(window);
		currentState = new MainMenuState();
	}
	public void run() {
		Clock clock = new Clock();
		Time lastTime = Time.getSeconds(0);
		Time currentTime = Time.getSeconds(0);
		
		while(window.isOpen() && currentState != null )		{
			
			for(Event event : window.pollEvents())	{
				currentState.handleEvent(event);
				
				if( event.type == Event.Type.CLOSED)	{
					window.close();
				}
				
				if(event.type == Event.Type.RESIZED)	{
					
					
					//skalowanie okna
					window.setView(new View(new FloatRect(0,0, ((SizeEvent)event).size.x,((SizeEvent)event).size.y) ) );
					//State.setWindowSize(window.getSize().x, window.getSize().y );
				
				}
			}
			lastTime = currentTime;
			currentTime = clock.getElapsedTime();
			
		
			currentState = currentState.update(Time.sub(currentTime, lastTime).asSeconds());
			
			if(currentState != null)	{
				currentState.draw(window);
			}
				
			window.display();
			window.clear();
		}
	
			
	}
	
}
