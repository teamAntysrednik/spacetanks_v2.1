package spaceTanks.rendering.gui;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.ConstFont;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.Text;
import org.jsfml.graphics.Transformable;
import org.jsfml.window.event.Event;

public class Button extends Widget {

	private Text button = new Text();
	private Drawable buttonBackground;
	private int buttonX, buttonY;
	private ICallable afterPush;
	
	//czy jest wcisnieta
	private boolean ifPress;
	//czy byla wywo�ana od wcisniecia
	private boolean ifWasCalled;
	private boolean repeat;
	
	
	public Button()	{
		ifPress = false;
		ifWasCalled = false;
		repeat = false;
	}
	
	public Button(int posX, int posY, String str, ConstFont font, int size, Color color, int style)	{
		button.setString(str);
		button.setCharacterSize(size); 
		button.setColor(color);
		button.setStyle(style);
		button.setFont(font);
		
		setButtonCenter(posX, posY);
		
		recalculatebuttonOrigin();
		ifPress = false;
		ifWasCalled = false;
		repeat = false;
		
	}
	public void setRepeat(boolean f)	{
		repeat = f;
	}
	
	@Override
	public void handleEvent(Event event) {
		if(	event.type == Event.Type.MOUSE_BUTTON_PRESSED)	{
			float mouseClickX = event.asMouseButtonEvent().position.x;
			float mouseClickY =  event.asMouseButtonEvent().position.y;
			
			if(	position.getX() - buttonX <= mouseClickX && mouseClickX <= position.getX() + buttonX  &&
				position.getY() - buttonY <= mouseClickY && mouseClickY <= position.getY() + buttonY  	)
			{
				setIfPress(true);
			}
		}
		else if( event.type == Event.Type.MOUSE_BUTTON_RELEASED )	{
				setIfPress(false);
				ifWasCalled = false;
						
		}
	}

	@Override
	public void update(float dt) {
		if( ifPress )	{
			if(!repeat && !ifWasCalled)	{
				ifWasCalled = true;
				afterPush.toCallAfterPush(dt);				
			}
			else	{
				afterPush.toCallAfterPush(dt);				
			}
		}
		
	}

	@Override
	public void draw(RenderWindow window) {
		if(buttonBackground != null)
		{
			window.draw(buttonBackground);
		}
		window.draw(button);

	}

	protected void countCenter()	{
		
	}
	@Override
	protected void updatePosition() {
		button.setPosition(position.getX(), position.getY());
		if(buttonBackground != null && buttonBackground instanceof Transformable)	{
			((Transformable) buttonBackground).setPosition(position.getX(), position.getY());
		}
	}
	
	public void setICallable(ICallable x)	{
		afterPush = x;
	}
	public void setButtonCenter(int x, int y)
	{
		buttonX = x;
		buttonY = y;
	}
	public void setbutton(String str)	{
		button.setString(str);
		recalculatebuttonOrigin();
	}
	
	
	private void recalculatebuttonOrigin() {
		button.setOrigin(button.getLocalBounds().left + button.getLocalBounds().width / 2, 
		button.getLocalBounds().top + button.getLocalBounds().height / 2);
		
	}

	public void setFonts(ConstFont font)	{
		button.setFont(font);
		recalculatebuttonOrigin();

	}
	
	public void setCharacterSize(int size)	{
		button.setCharacterSize(size); 
		recalculatebuttonOrigin();

	}
	
	public void setColor(Color color)	{
		button.setColor(color);
	}
	
	public void setStyle(int style)	{
		button.setStyle(style);
		recalculatebuttonOrigin();
	}
	
	public void setIfPress(boolean f) {
		ifPress = f;
	}
	
	public void setReset(boolean f) {
		ifWasCalled = f;
	}
	
	
	public void setButtonBackground(Drawable background)	{
		
		buttonBackground = background;
	}
	
	public void setButtonBackgroundAfterClick()	{
		buttonBackground = new RectangleShape(new org.jsfml.system.Vector2f(300, 40) );
		((RectangleShape) buttonBackground).setFillColor(Color.BLACK);;
		((RectangleShape) buttonBackground).setOutlineColor(Color.WHITE);
		((RectangleShape) buttonBackground).setOutlineThickness(1);
		((RectangleShape) buttonBackground).setOrigin(
				((RectangleShape) buttonBackground).getLocalBounds().width / 2, 
				((RectangleShape) buttonBackground).getLocalBounds().height / 2);
		((RectangleShape) buttonBackground).setPosition(position.getX(), position.getY());
		
		
	}
	
	public void cancelBackgroundAfterClick()	{
		buttonBackground = null;
	}
	
}
