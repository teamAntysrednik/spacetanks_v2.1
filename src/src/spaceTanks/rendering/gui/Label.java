package spaceTanks.rendering.gui;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.ConstFont;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.Text;
import org.jsfml.graphics.Transformable;
import org.jsfml.window.event.Event;

public class Label extends Widget {
	
	private Text label = new Text();
	private Drawable labelBackground; 
	
		
	public Label()	{
		
	}
	
	public Label(String str, ConstFont font, int size, Color color, int style)	{
		label.setString(str);
		label.setCharacterSize(size); 
		label.setColor(color);
		label.setStyle(style);
		label.setFont(font);

		recalculateLabelOrigin();
	}
	
	@Override
	public void handleEvent(Event event) {
		
	}

	@Override
	public void update(float dt) {		
	}

	@Override
	public void draw(RenderWindow window) {
		window.draw(label);
		if(labelBackground != null)
		{
			window.draw(labelBackground);
		}
	}
	
	public void setLabel(String str)	{
		label.setString(str);
		recalculateLabelOrigin();
	}
	
	
	public void setFonts(ConstFont font)	{
		label.setFont(font);
		recalculateLabelOrigin();

	}
	
	public void setCharacterSize(int size)	{
		label.setCharacterSize(size); 
		recalculateLabelOrigin();

	}
	
	public void setColor(Color color)	{
		label.setColor(color);
	}
	
	public void setStyle(int style)	{
		label.setStyle(style);
		recalculateLabelOrigin();
	}
	
	private void recalculateLabelOrigin()	{
		label.setOrigin(label.getLocalBounds().left + label.getLocalBounds().width / 2, 
						label.getLocalBounds().top + label.getLocalBounds().height / 2);
	}
	
	public void setLabelBackground(Drawable background)	{
		labelBackground = background;
	}
	
	@Override
	protected void updatePosition() {
		label.setPosition(position.getX(), position.getY());
		if(labelBackground != null && labelBackground instanceof Transformable)	{
			((Transformable) labelBackground).setPosition(position.getX(), position.getY());
		}
	}
	
	
	
	
	
}
