package spaceTanks.rendering.gui;

import org.jsfml.graphics.RenderWindow;
import org.jsfml.window.event.*;

import spaceTanks.utilities.Vector2f;

abstract class Widget {
	
	Vector2f position = new Vector2f();
	Vector2f offSet = new Vector2f();
	Vector2f parentPosition = new Vector2f();
	
	AnchorMode anchor = AnchorMode.NONE;
	
	public abstract void handleEvent(Event event);
	public abstract void update(float dt);
	public abstract void draw(RenderWindow window);
	
	//rescape();
	
	/*public void setPosition(int positionX, int positionY){
		position.set(positionX, positionY);
		updatePosition();
	}*/
	
	public Vector2f getPosition()	{
		return position;
	}
	public void setOffSet(int offSetX, int offSetY)	{
		offSet.set(offSetX, offSetY);
		recalculatePosition();
		
	}
	
	public void setParentPosition(Vector2f vector)	{
		parentPosition = vector;
		recalculatePosition();
	}
	
	
	public void setAnchorMode(AnchorMode anchor)	{
		this.anchor = anchor;
	}
	
	protected abstract void updatePosition();
	
	private void recalculatePosition()	{
		if(anchor == AnchorMode.NONE)	{
			position = Vector2f.add(parentPosition, offSet);
		}
		else	{
			int x = 0, y = 0;
			x += ((anchor.getValue() & 8 ) != 0 ? (parentPosition.getX() / 2) : 0);
			x += ((anchor.getValue() & 4) != 0 ? parentPosition.getX() : 0);
			
			y += ((anchor.getValue() & 2) != 0 ? (parentPosition.getY() / 2) : 0);
			y += ((anchor.getValue() & 1) != 0 ? parentPosition.getY() : 0);
			
			position = new Vector2f(x + offSet.getX(), y + offSet.getY());
		}
		updatePosition();
	}
	
	
}
