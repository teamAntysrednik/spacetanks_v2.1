package spaceTanks.rendering.gui;

public enum AnchorMode {
	NONE (16),
	MIDDLE (10) ,
	UP (8),
	DOWN (9),
	LEFT (2), 
	RIGHT (6),
	UPPER_LEFT (0),
	UPPER_RIGHT (4),
	LOWER_LEFT (1),
	LOWER_RIGHT (5);
	int value;
	
	AnchorMode(int x)	{
		value = x;						
	}
	int getValue()	{
		return value;
	}
}
