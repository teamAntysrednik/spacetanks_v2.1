package spaceTanks.rendering.gui;

import java.util.ArrayList;
import java.util.List;

import org.jsfml.graphics.RenderWindow;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.SizeEvent;

import spaceTanks.utilities.Vector2f;

public class Canvas {
	
	List<Widget> widgets = new ArrayList<Widget>();
	float scale = 1;
	Vector2f dimensions = new Vector2f();
	
	public Canvas(int width, int height)	{
		dimensions.set(width, height);
	}
	
	public void handleEvent(Event event)	{
		
		if(event.type == Event.Type.RESIZED)	{
			SizeEvent sizeEvent = (SizeEvent) event;
			dimensions.set(sizeEvent.size.x, sizeEvent.size.y);
			for(Widget widget : widgets){
				widget.setParentPosition(dimensions);
			}
			
		}
		
		for(Widget widget : widgets){
			widget.handleEvent(event);
		}
		
	}
	
	public void update(float dt)	{
		for(Widget widget : widgets){
			widget.update(dt);
		}
		
	}
	
	public void draw(RenderWindow window)	{
		for(Widget widget : widgets){
			widget.draw(window);
		}
		
	}
	
	public void addWidget(Widget widget)	{
		widget.setParentPosition(dimensions);
		widgets.add(widget);
	}
	
}
