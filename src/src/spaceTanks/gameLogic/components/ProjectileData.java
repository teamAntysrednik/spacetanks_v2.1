package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;

public class ProjectileData extends Component{
	int damage;
	float aliveTime;
	
	public ProjectileData(GameEntity owner) {
		super(owner);
	}
	
	public void setDamage(int newDamage){
		damage = newDamage;
	}
	
	public void setAliveTime(float newAliveTime){
		aliveTime = newAliveTime;
	}
	
	public int getDamage(){
		return damage;
	}
	
	public float getAliveTime(){
		return aliveTime;
	}
	
	
}
