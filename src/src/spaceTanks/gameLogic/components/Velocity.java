package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;
import spaceTanks.utilities.Vector2f;

public class Velocity extends Component{
	Vector2f velocity = new Vector2f();

	
	public Velocity(GameEntity owner) {
		super(owner);
	}
	
	public void setVelocity(float newValueX, float newValueY){
		velocity.set(newValueX, newValueY);
	}
	
	public Vector2f getVelocity(){
		return velocity;
	}

	
}
