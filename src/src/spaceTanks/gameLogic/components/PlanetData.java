package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;

public class PlanetData extends Component{
	float radius;
	
	
	public PlanetData(GameEntity owner) {
		super(owner);
	}
	
	public void setRadius(float radius)
	{
		this.radius = radius;
	}
	
	public float getRadius(){
		return radius;
	}

}
