package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;

public class Component {
	protected GameEntity owner;
	
	protected Component(GameEntity gameEntity){
		owner = gameEntity;
	}
}