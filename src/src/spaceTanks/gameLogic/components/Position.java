package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;

public class Position extends Component{
	float x, y;
	
	public Position(GameEntity owner) {
		super(owner);
	}
	
	public void setPosition(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	Boolean checkPositionCorrectness(float x, float y){
		return true;
	}
	
	public float getPositionX(){
		return x;
	}
	
	public float getPositionY(){
		return y;
	}
}