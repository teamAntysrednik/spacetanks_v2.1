package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;

public class Collider extends Component{
	float radius;
	public Collider(GameEntity owner){
		super(owner);
	}
	
	public void setCollider(float radius){
		this.radius = radius;
	}
	
	public float getRadius(){
		return radius;
	}
	
}
