package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;

public class TankData extends Component{
	float angle;
	int hp;
	GameEntity connectedPlanet;
	float cannonAngle = 0;
	int idNumber;
	static int idNumberSet = 0;
	
	public TankData(GameEntity owner) {
		super(owner);
	}
		
	public void setTankData(float angle, int hp, GameEntity entity){
		this.angle = angle;
		this.hp = hp;
		this.connectedPlanet = entity;
		this.idNumber = idNumberSet;
		idNumberSet++;
	}
	
	public void setAngle(float newAngle){
		this.angle = newAngle;
	}
	
	public void setCannonAngle(float newAngle){
		this.cannonAngle = newAngle;
	}
	
	public void setHp(int newHp){
		hp = newHp;
	}
	
	public float getAngle(){
		return angle;
	}
	
	public int getHp(){
		return hp;
	}
	
	public float getCannonAngle(){
		return cannonAngle;
	}
	
	public GameEntity getTankPlanet(){
		return connectedPlanet;
	}
	
	public int getIdNumber(){
		return idNumber;
	}
}
