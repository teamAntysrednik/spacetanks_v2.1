package spaceTanks.gameLogic.components;

import spaceTanks.gameLogic.GameEntity;

public class Mass extends Component{
	float massValue;
	
	public Mass(GameEntity owner) {
		super(owner);
	}
	
	public void setMass(float value){
		this.massValue = value;
	}
	
	public float getMass(){
		return massValue;
	}
}
