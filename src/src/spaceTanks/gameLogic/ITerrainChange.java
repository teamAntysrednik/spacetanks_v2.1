package spaceTanks.gameLogic;

import org.jsfml.graphics.Texture;

public interface ITerrainChange {
	void callback(Texture texture);

}
