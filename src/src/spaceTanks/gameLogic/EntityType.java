package spaceTanks.gameLogic;

public enum EntityType {
	TANK(),
	PROJECTILE(),
	PLANET();
}
