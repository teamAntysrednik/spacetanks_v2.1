package spaceTanks.gameLogic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

import org.jsfml.graphics.Color;

import spaceTanks.utilities.Vector2f;

public class GameWorld {
	private int cPlayers; //liczba graczy
	private int currentPlayer = 0;
	boolean ifSimulation = false;
	private Terrain terrain;
	
	public	ArrayList<GameEntity> listOfTanks = new ArrayList<GameEntity>();
	public	ArrayList<GameEntity> listOfPlanets = new ArrayList<GameEntity>();
	public ArrayList<GameEntity> listOfProjectiles = new ArrayList<GameEntity>();
	public ArrayList<GameEntity> entities = new ArrayList<GameEntity>();
	private MapSize mapSize;
	
	public GameWorld(MapSize mapSize, int cPlayers){
		this.mapSize = mapSize;
		this.cPlayers = cPlayers;
		terrain = new Terrain(mapSize.getX(), mapSize.getY());
		generatePlanets();
		generateTanks();
	}
	
	public int getCPlayers()	{
		return cPlayers;
	}
	
	public int getPanelHeight(){
		return mapSize.getY();
	}
	
	public int getPanelWidth(){
		return mapSize.getX();
	}
	
	public Terrain getTerrain(){
		return terrain;
	}

	private void generatePlanets(){
		float r, posX, posY, m, density, area;
		boolean flag;
		for(int i = 0; i < cPlayers + 2; ++i){
			do{
				flag = false;
				r =  (float) ThreadLocalRandom.current().nextDouble(mapSize.getMinRadius(), mapSize.getMaxRadius());
				posX = (float) ThreadLocalRandom.current().nextDouble(-mapSize.getX()/2 + r + 20, mapSize.getX()/2 -r-20);
				posY = (float) ThreadLocalRandom.current().nextDouble(-mapSize.getY()/2 + r + 20, mapSize.getY()/2 -r-20);
				density = (float) ThreadLocalRandom.current().nextDouble(mapSize.getMinDensity(), mapSize.getMaxDensity());
				//area = ;
				m = (float) (Math.pow(r, 1.33) * density);
				
				//m = (float) ThreadLocalRandom.current().nextDouble(6500, 8000);
				
				for(int j = 0; j < i; ++j){
					float rr, xx, yy;
					GameEntity  tmpEntity = listOfPlanets.get(j);
					rr = tmpEntity.planetData.getRadius();
					xx = tmpEntity.position.getPositionX();
					yy = tmpEntity.position.getPositionY();
					float distance = (posX - xx)*(posX - xx) + (posY - yy)*(posY - yy);
					distance = (float) Math.sqrt(distance);
					if(distance < r + rr + 60){
						flag = true;
						break;
					}
				}
				
			}while(flag);
			
			terrain.createCircle((int) posX + mapSize.getX() / 2, (int) posY + mapSize.getY() / 2, r, Color.WHITE);
			
			GameEntity entity = EntityFactory.createPlanet();
			entity.mass.setMass(m);
			entity.position.setPosition(posX, posY);
			entity.planetData.setRadius(r);
			entity.collider.setCollider(r);
			listOfPlanets.add(entity);
		}

	}
	
	private void generateTanks(){
		for(int i = 0; i < cPlayers; ++i){
			GameEntity conPlanet = listOfPlanets.get(i);
			float angle;
			angle = ThreadLocalRandom.current().nextFloat() * 360;
			float vecX, vecY;
			float posX, posY;
			float rad = (angle * (float) Math.PI) /180;
			vecX = (float) Math.cos(rad) * (conPlanet.planetData.getRadius() + 5);
			vecY = (float) Math.sin(rad) * (conPlanet.planetData.getRadius() + 5);
			posX = conPlanet.position.getPositionX() + vecX;
			posY = conPlanet.position.getPositionY() + vecY;
			
			GameEntity entity = EntityFactory.createTank();
			
			entity.position.setPosition(posX, posY); 
			entity.tankData.setTankData(angle, 100, conPlanet);
			entity.collider.setCollider(12);
			listOfTanks.add(entity);
			
		}
	}
	
	public void moveTank(boolean ifLeft, float deltaTime){
		if(ifSimulation)return;
		float crrPosX;
		float crrPosY;
		float multiply = mapSize.getMultiply();
		
		
		GameEntity tank = listOfTanks.get(currentPlayer);
		float planetPosX = tank.tankData.getTankPlanet().position.getPositionX();
		float planetPosY = tank.tankData.getTankPlanet().position.getPositionY();
		float planetRadius = tank.tankData.getTankPlanet().planetData.getRadius();
		float tankAngle = tank.tankData.getAngle();
		float angle = (180* (multiply / (planetRadius / 3.141592f)) * deltaTime);
		
		if(ifLeft){
			tankAngle = (tankAngle + 360 - angle) % 360;
		}
		else{
			tankAngle = (tankAngle + angle) % 360;
		}
		
		float vecX, vecY;
		float rad = (tankAngle * (float) Math.PI) /180;
		vecX = (float) Math.cos(rad) * (planetRadius + 5);
		vecY = (float) Math.sin(rad) * (planetRadius + 5);
		crrPosX = planetPosX + vecX;
		crrPosY = planetPosY + vecY;
		
		tank.position.setPosition(crrPosX, crrPosY);
		tank.tankData.setAngle(tankAngle);
	}
	
	public void moveCannon(boolean ifLeft, float deltaTime){
		if(ifSimulation)return;
		float angle = listOfTanks.get(currentPlayer).tankData.getCannonAngle();
		float multiply = 20;
		float newAngle = multiply * deltaTime;
		
		if(ifLeft){
			if(angle - newAngle <= -90){
				angle = -90;
			}
			else{
				angle -= newAngle;
			}
		}
		else{
			if(angle + newAngle >= 90){
				angle = 90;
			}
			else{
				angle += newAngle;
			}
		}
		listOfTanks.get(currentPlayer).tankData.setCannonAngle(angle);
	}
	
	public void shoot(){
		if(ifSimulation)return;
		GameEntity tank = listOfTanks.get(currentPlayer);
		GameEntity projectile = EntityFactory.createProjectile();
		projectile.mass.setMass(75);
		float projectileAngle = tank.tankData.getAngle() + tank.tankData.getCannonAngle();
		Vector2f norm = new Vector2f(1, 0);
		norm.rotation(projectileAngle * (float)Math.PI / 180, new Vector2f());
		projectile.velocity.setVelocity(250 * norm.getX(), 250 * norm.getY());
		projectile.position.setPosition(tank.position.getPositionX() + 15*norm.getX(), tank.position.getPositionY() + 15*norm.getY());
		projectile.collider.setCollider(2);
		projectile.projectileData.setDamage(34);
		projectile.projectileData.setAliveTime(20);
		listOfProjectiles.add(projectile);
		ifSimulation = true;
		currentPlayer = (currentPlayer + 1) % cPlayers;
	}
	
	public boolean checkEnd(){
		if(listOfTanks.size() < 2)return true;
		return false;
	}
	
	public int getCurrentPlayer()
	{
		return currentPlayer;
	}
	
	
	public void update(float deltaTime){
		if(!ifSimulation) return;
		
		GameEntity projectile = listOfProjectiles.get(0);
		Vector2f lastPos = new Vector2f(projectile.position.getPositionX(), projectile.position.getPositionY());
		
		projectile.projectileData.setAliveTime(projectile.projectileData.getAliveTime() - deltaTime);
		if(projectile.projectileData.getAliveTime() <= 0){
			ifSimulation = false;
			listOfProjectiles.remove(0);
			return;
		}
		Vector2f vec = new Vector2f();
		//wyliczenie sily
		for(GameEntity planet : listOfPlanets){
			Vector2f distance = Vector2f.subtract(new Vector2f(planet.position.getPositionX(), planet.position.getPositionY()), 
					new Vector2f(projectile.position.getPositionX(), projectile.position.getPositionY()));
			float vecLength = distance.length();
			float fg = 250*(planet.mass.getMass() * projectile.mass.getMass())/(vecLength * vecLength);
			distance.normalize();
			vec.add(distance.multiply(fg));
		}
		
		//wyliczenie 
		projectile.velocity.getVelocity().add(vec.multiply(deltaTime/projectile.mass.getMass()));
		vec = projectile.velocity.getVelocity().multi(deltaTime);
		projectile.position.setPosition(projectile.position.getPositionX() + vec.getX(), 
		projectile.position.getPositionY() + vec.getY());
		
		//sprawdzanie kolizji
		for(Iterator<GameEntity> it = listOfTanks.iterator(); it.hasNext();){
			GameEntity tank = it.next();
			float distanceX = (tank.position.getPositionX() - projectile.position.getPositionX());
			float distanceY = (tank.position.getPositionY() - projectile.position.getPositionY());
			float distance = distanceX*distanceX + distanceY*distanceY;
			distance = (float) Math.sqrt(distance);
			if(distance <= tank.collider.getRadius() + projectile.collider.getRadius()){
				int hp = tank.tankData.getHp() - projectile.projectileData.getDamage();
				if(hp <= 0){
					cPlayers--;
					currentPlayer = currentPlayer % cPlayers;
					it.remove();
				}
				else{
					tank.tankData.setHp(hp);
				}
				ifSimulation = false;
				listOfProjectiles.remove(0);
				return;
			}
		}
		
		Vector2f collisionPoint = terrain.checkLineCollision((int) lastPos.getX() + mapSize.getX() / 2, (int) lastPos.getY() + mapSize.getY() / 2, (int) projectile.position.getPositionX() + mapSize.getX() / 2, (int) projectile.position.getPositionY() + mapSize.getY() / 2);
		
		if(collisionPoint != null){
			ifSimulation = false;
			terrain.createCircle((int) projectile.position.getPositionX() + mapSize.getX() / 2, (int) projectile.position.getPositionY() + mapSize.getY() / 2, 10, Color.BLACK);
			listOfProjectiles.remove(0);
			return;
		}
		
		/*for(Iterator<GameEntity> it = listOfPlanets.iterator(); it.hasNext();){
			GameEntity planet = it.next();
			float distanceX = (planet.position.getPositionX() - projectile.position.getPositionX());
			float distanceY = (planet.position.getPositionY() - projectile.position.getPositionY());
			float distance = distanceX*distanceX + distanceY*distanceY;
			distance = (float) Math.sqrt(distance);
			if(distance <= planet.collider.getRadius() + projectile.collider.getRadius()){
				ifSimulation = false;
				terrain.createCircle((int) projectile.position.getPositionX() + mapSize.getX() / 2, (int) projectile.position.getPositionY() + mapSize.getY() / 2, 10, Color.BLACK);
				listOfProjectiles.remove(0);
				return;
			}
		}*/
		
	}
	

	
	/*
	 * done---------------------poruszanie czolgiem
	 * done---------------------obracanie dziala -||-
	 * done---------------------strzelanie
	 * done---------------------czy koniec gry
	 * funkcja update tylko wtedy gdy symulacja
	 * ogarnianie tur
	 * 
	 * 
	 */

	
}
