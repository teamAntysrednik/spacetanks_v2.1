package spaceTanks.gameLogic;

import spaceTanks.gameLogic.components.*;

public class GameEntity {
	public Position position;
	public Mass mass;
	public Velocity velocity;
	public TankData tankData;
	public ProjectileData projectileData;
	public PlanetData planetData;
	public Collider collider;
}
