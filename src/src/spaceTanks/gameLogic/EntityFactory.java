package spaceTanks.gameLogic;

import spaceTanks.gameLogic.components.*;

public class EntityFactory {

	public static GameEntity createTank(){
		GameEntity entity = new GameEntity();
		
		entity.position = new Position(entity);
		entity.tankData = new TankData(entity);
		entity.collider = new Collider(entity);
		
		return entity;
		
	}
	public static GameEntity createPlanet(){
		GameEntity entity = new GameEntity();
		
		entity.mass = new Mass(entity);
		entity.position = new Position(entity);
		entity.planetData = new PlanetData(entity);
		entity.collider = new Collider(entity);
		
		return entity;
	}
	
	public static GameEntity createProjectile(){
		GameEntity entity = new GameEntity();
		
		entity.mass = new Mass(entity);
		entity.position = new Position(entity);
		entity.projectileData = new ProjectileData(entity);
		entity.velocity = new Velocity(entity);
		entity.collider = new Collider(entity);
		
		return entity;
	}
	
}
