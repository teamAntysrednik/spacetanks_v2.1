package spaceTanks.gameLogic;

public enum MapSize {
	 TINY(800, 600, 44, 79, 40f, 50.0f, 4.5f),
	 SMALL(1000, 750, 60, 99, 35f, 45.0f, 5.0f),
	 MEDIUM(1500, 1200, 110, 145, 30f, 40.0f, 5.6f),
	 LARGE(2000, 1600, 175, 230, 25f, 35.0f, 6.3f),
	 HUGE(2500, 2000, 180, 250, 20f, 30.0f, 7.5f);
	 
	 private int x, y;
	 private float minRadius, maxRadius;
	 private float minDensity, maxDensity;
	 private float multiply;
	 
	 MapSize(int x, int y, float minR, float maxR, float minD, float maxD, float multiply){
		 this.x = x;
		 this.y = y;
		 this.maxRadius = maxR;
		 this.minRadius = minR;
		 this.minDensity = minD;
		 this.maxDensity = maxD;
		 this.multiply = multiply;
	 }
	 
	 public int getX(){
		 return x;
	 }
	 
	 public int getY(){
		 return y;
	 }
	 
	 public float getMinRadius(){
		 return minRadius;
	 }
	 
	 public float getMaxRadius(){
		 return maxRadius;
	 }
	 
	 public float getMinDensity(){
		 return minDensity;
	 }
	 
	 public float getMaxDensity(){
		 return maxDensity;
	 }
	 
	 public float getMultiply(){
		 return multiply;
	 }
}
