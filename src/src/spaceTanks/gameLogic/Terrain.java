package spaceTanks.gameLogic;

import org.jsfml.graphics.Color;
import org.jsfml.graphics.Image;
import org.jsfml.graphics.Texture;
import org.jsfml.graphics.TextureCreationException;

import spaceTanks.utilities.Vector2f;

public class Terrain {
	Image image = new Image();
	ITerrainChange terrainCallback;
	Texture texture = new Texture();
	
	public Terrain(int x, int y) {
		image.create(x, y, Color.BLACK);
		try {
			texture.loadFromImage(image);
		} catch (TextureCreationException e) {
			e.printStackTrace();
		}
	}
	
	boolean ifInRadius(int x, int y, int xx, int yy, float r){
		float distance = (xx-x)*(xx-x) + (yy-y)*(yy-y);
		distance = (float) Math.sqrt(distance);
		if(distance <= r){
			return true;
		}
		return false;
	}
	
	public void createCircle(int x, int y, float r, Color c){
		for(int i =  (int) (y-r); i <= (int) (y+r); ++i){
			for(int j = (int) (x-r); j <= (int) (x+r); ++j){
				if(ifInRadius(x, y, j, i, r)){
					image.setPixel(j, i, c);
				}
			}
		}

		texture.update(image, 0, 0);
		callCallback();
	}
	
	public void callCallback()
	{
		if(terrainCallback != null){
			terrainCallback.callback(texture);
		}
	}
	
	public void setCallback(ITerrainChange iTerrainChange){
		terrainCallback = iTerrainChange;
	}
	
	public Vector2f checkLineCollision(int x0, int y0, int x1, int y1){
		if(x0 < 0 || x0 >= image.getSize().x || y0 < 0 || y0 >= image.getSize().y ||
			x1 < 0 || x1 >= image.getSize().x || y1 < 0 || y1 >= image.getSize().y)
		{
			return null;
		}
		
		if(x0 == x1){
			if(y0 > y1){
				int yTmp = y0;
				y0 = y1;
				y1 = yTmp;
			}
			
			for(int i = y0; i < y1; ++i){
				if(image.getPixel(x0, i).equals(Color.WHITE)){
					return new Vector2f(x0, i);
				}
			}
			return null;
		}
		
		if(x0 >x1){
			int xTmp = x0;
			x0 = x1;
			x1 = xTmp;
			
			int yTmp = y0;
			y0 = y1;
			y1 = yTmp;
		}
		
		
		for(int i = x0; i < x1; ++i){
			int j = ((y1-y0) / (x1-x0))*(i - x0) + y0;
			if(image.getPixel(i, j).equals(Color.WHITE)){
				return new Vector2f(i, j);
			}
		}
		
		return null;
		
	}
}
