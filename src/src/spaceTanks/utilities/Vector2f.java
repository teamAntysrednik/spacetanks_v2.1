package spaceTanks.utilities;

public class Vector2f
{
	private float x;
	private float y;

	public Vector2f()
	{
		x = 0;
		y = 0;
	}

	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2f(Vector2f v)
	{
		x = v.getX();
		y = v.getY();
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public Vector2f add(Vector2f v)
	{
		x += v.getX();
		y += v.getY();
		return this;
	}

	public static Vector2f add(Vector2f v, Vector2f w)
	{
		return new Vector2f(v.getX() + w.getX(), v.getY() + w.getY());
	}

	public Vector2f subtract(Vector2f v)
	{
		x -= v.getX();
		y -= v.getY();
		return this;
	}

	public static Vector2f subtract(Vector2f v, Vector2f w)
	{
		return new Vector2f(v.getX() - w.getX(), v.getY() - w.getY());
	}

	public Vector2f multiply(float factor)
	{
		x *= factor;
		y *= factor;
		return this;
	}

	public Vector2f multi(float factor)
	{
		return new Vector2f(x * factor, y * factor);
	}

	public static float scalarProduct(Vector2f v, Vector2f w)
	{
		return (v.getX() * w.getX()) + (v.getY() * w.getY());
	}

	public static float vectorProduct(Vector2f v, Vector2f w)
	{
		return (v.getX() * w.getY()) + (v.getY() * w.getX());
	}

	public void rotation(float angle, Vector2f displacment)
	{
		this.subtract(displacment);

		float cosinus = (float) Math.cos(angle);
		float sinus = (float) Math.sin(angle);

		float newX = cosinus * x - sinus * y;
		float newY = sinus * x + cosinus * y;

		x = newX;
		y = newY;

		this.add(displacment);
	}

	public float length()
	{
		return (float) Math.sqrt(scalarProduct(this, this));
	}

	public float squaredLength()
	{
		return scalarProduct(this, this);
	}

	public void normalize()
	{
		float len = length();
		x /= len;
		y /= len;
	}

	public Vector2f normalized()
	{
		Vector2f v = new Vector2f(this);
		v.normalize();
		return v;
	}
}